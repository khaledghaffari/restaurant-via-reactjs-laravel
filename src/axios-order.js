// import axios from 'axios'

// const instance = axios.create({
//   baseURL: 'https://sandwich-maker-388c0.firebaseio.com/'
// })

// export default instance;

// //'https://sandwich-maker-388c0.firebaseio.com/'
// //https://react-my-burger-2ba40.firebaseio.com/

import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://sandwich-maker-388c0.firebaseio.com/'
})

export default instance;