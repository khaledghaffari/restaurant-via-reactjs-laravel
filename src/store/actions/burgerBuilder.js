import * as actionTypes from './actions';





export const moveIngredient = (dragIndex, hoverIndex, config) => {
    return {
        type: actionTypes.MOVE_INGREDIENT,
        dragIndex,
        hoverIndex,
        ingredient: config
    }
}