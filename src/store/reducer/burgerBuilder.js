import * as actionTypes from '../actions/actions';
import {updateObject} from '../util';

const initialState = {
    ingredients: [],
    selectedIngredients: [],
    error: false,
    totalPrice: 4
}


const moveIngredient = (state, action) => {
    let config = action.ingredient;
    let dragIndex = action.dragIndex;
    let hoverIndex = action.hoverIndex;
    let dragIngredient = state.selectedIngredients[dragIndex];
    let newPrice = state.totalPrice;

    let newIngredients = state.selectedIngredients.concat();

    if (newIngredients.length === dragIndex) {
        dragIngredient = config;
        newPrice = state.totalPrice + config.price;
    } else {
        newIngredients.splice(dragIndex, 1);
    }

    newIngredients.splice(hoverIndex, 0, dragIngredient);

    return updateObject(state, {
        selectedIngredients: newIngredients,
        totalPrice: newPrice
    });
}
const reducer = (state = initialState, action) => {
    switch (action.type) {
       
        case actionTypes.MOVE_INGREDIENT: return moveIngredient(state, action)
       
        default: return state;
    }
}

export default reducer;
