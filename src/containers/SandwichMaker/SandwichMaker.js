import React, {Component} from 'react';
import {connect} from 'react-redux'
import Aux from '../../hoc/Auxilary'
import Sandwich from '../../components/Sandwich/Sandwich'
import BuildControls from '../../components/Sandwich/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Sandwich/OrderSummary/OrderSummary'
import axios from '../../axios-order'
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import TableFoof from '../../components/Sandwich/TableFood/TableFood'
import * as actionTypes from '../../store/actions'
import * as actions from '../../store/actions/actions';
import { DragDropContext } from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend'

class SandwichMaker extends Component {

  state = {
    purchasing: false,
    loading: false,
    error: false
  }
  
  componentDidMount() {
    // axios.get('https://sandwich-maker-388c0.firebaseio.com/ingredients.json')
    //   .then(response => {
    //     this.setState({ingredients: response.data});
    //   })
    //   .catch(error => {
    //     this.setState({error: true})
    //   });
  }

  

  updatePurchaseState (ingredients) {
    
    const sum = Object.keys(ingredients)
        .map(igKey => {
          return ingredients[igKey];
        })
        .reduce((sum, el) => {
          return sum + el;
        }, 0);

    return sum > 0;
  }

  purchaseHandler = () => {
    this.setState({purchasing: true})
  }

  purchaseCancelHandler = () => {
    this.setState({purchasing: false})
  }

  purchaseContinueHandler = () => {

    this.props.history.push('/checkout');
  }

  render() {

    const disableInfo = {
      ...this.props.ings
    };
    for (let key in disableInfo) {
      disableInfo[key] = disableInfo[key] <= 0
    }

    let orderSummary = null;

    if(this.props.ings) {
      orderSummary =  <OrderSummary 
      purchaseCancelled={this.purchaseCancelHandler}
      purchaseContinued={this.purchaseContinueHandler}
      price={this.props.price}
      ingredients={this.props.ings} />
    }

    if (this.state.loading) {
      orderSummary = this.state.error ? <p>Ingredients Can't be loaded!</p> : <Spinner />
    }
    

    let sandwich = <Spinner />

    if(this.props.ings) {
      sandwich =
      <Aux>
      <div style={{float:'right',marginRight:'4em' }} ><TableFoof/></div>
      <div style={{marginLeft:'30em'}}> 
      <Sandwich 
      ingredients={this.props.ings} 
      drag
      moveIngredient={this.props.onMoveIngredient}
     />  
      </div>
       
        <BuildControls 
          ingredientAdded={this.props.onIngredientAdded}
          ingredientRemoved={this.props.onIngredientRemoved}
          disabled={disableInfo}
          price={this.props.price}
          purchasable={this.updatePurchaseState(this.props.ings)}
          ordered={this.purchaseHandler}
        />
        
      </Aux> 
      
    }
    

    return (
      <Aux>
       
        {sandwich}

        <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
         {orderSummary}
        </Modal>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return {
    ings: state.ingredients,
    price: state.totalPrice
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onIngredientAdded: (ingName) => dispatch({type: actions.ADD_INGREDIENT, ingredientName: ingName}),
    onIngredientRemoved: (ingName) => dispatch({type: actions.REMOVE_INGREDIENT, ingredientName: ingName}),
   
    onMoveIngredient: (dragIndex, hoverIndex, config) => dispatch(actionTypes.moveIngredient(dragIndex, hoverIndex, config)),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(SandwichMaker, axios));
//export default connect(mapStateToProps, mapDispatchToProps)(DragDropContext(HTML5Backend)(SandwichMaker));
//export default connect(mapStateToProps,mapDispatchToProps)(DragDropContext(HTML5Backend)(SandwichMaker, axios));