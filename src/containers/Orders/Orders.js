import React, {Component} from 'react';

import Order from '../../components/Order/Order'
import axios from '../../axios-order';

import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'

class Orders extends Component {

  state={
    order: [],
    loading: true
  }

  deleteOrderHandler(props){
    axios.delete('/orders.json'+props.match.params.id)
        .then(response => {
           // console.log(response);
           this.setState({loading: true, order: null});
        });
}
  componentDidMount() {
    axios.get('/orders.json').then(res => {
      const fetchedOrders = [];
      for (let key in res.data) {
        fetchedOrders.push({
          ...res.data[key],
        id: key
      })
      }
      this.setState({loading: false, order: fetchedOrders});
      console.log(res.data)
    })
    .catch(err => {
      this.setState({loading: false})
    })
  }

  
  render(){
    return (
      <div>
        {this.state.order.map(order => (
          <Order 
            key={order.id}
            ingredients={order.ingredients}
            price={+order.price}
            deleted={this.deleteOrderHandler} 
          />
        ))}
      </div>
    );
  }


}

export default withErrorHandler(Orders, axios);