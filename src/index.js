import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
// import 'bootstrap/dist/css/bootstrap.min.css';
// import $ from 'jquery';
// import Popper from 'popper.js';
import 'bootstrap/dist/css/bootstrap.min.css';
//import  'bootstrap / dist / css / bootstrap.min.css' ;
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducer from './store/reducer/reducer'
import './fontawesome';
const store = createStore(reducer);


const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
  
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
