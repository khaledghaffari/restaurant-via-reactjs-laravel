import React, { Component } from 'react'
import classes from './TableFood.css'
import tomato from '../../../assets/images-burger/exp3.png'
import meat from '../../../assets/images-burger/meat2.png'
import cheese from '../../../assets/images-burger/cheese.png'
import letuce from '../../../assets/images-burger/letuce.png'
export class TableFood extends Component {
    render() {
        return (
            <div >
                <div className={classes.grid}>
                      
                        <div className={classes.plate} > 
                            <div className={classes.after} >
                            
                            <img src={tomato} style={{width:'70px',margin:'18px'}} />
                            </div>
                                
                              
                           
                        </div>
                        <div className={classes.plate}  >
                            <div className={classes.after}>
                            <img src={letuce} style={{width:'130px',margin:'-10px'}} />
                                 </div> 
                        </div>
                                                   
                        <div className={classes.plate} > 
                            <div className={classes.after}>
                            <img src={meat} style={{ width:'110px',marginLeft:'8px',paddingRight:'15px',paddingTop:'30px' }} />
                            </div>
                        </div>
                        <div className={classes.plate} >
                            <div className={classes.after}> 
                            <img src={cheese} style={{width:'90px',margin:'10px'}} />
                            </div> 
                        </div>
                </div>
            </div>
           
        )
    }
}

export default TableFood
