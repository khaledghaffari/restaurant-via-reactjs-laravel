import React from 'react';
import NavigationItem from './NavigationItem/NavigationItem' 
import classes from './NavigationItems.css'
import {NavLink} from 'react-router-dom'
import { Icon, InlineIcon } from '@iconify/react';
import utensilsIcon from '@iconify/icons-fa-solid/utensils';
import caretDownOutlined from '@iconify/icons-ant-design/caret-down-outlined';


const navigationItems = () => (
  
  <ul className={classes.NavigationItems}>
     
  
   <ul className={classes.NavigationItems2}>
  
   <div className={classes.dropdown } >
      <NavLink to= '/carte'>
      <button className={classes.dropbtn}> <Icon icon={utensilsIcon}/> La Carte<Icon icon={caretDownOutlined} /></button>
      </NavLink>
     
        <div className={classes.dropdownContent} >
            <div className={classes.row } >
                  <div className={classes.column } >

                  <NavigationItem link="/hamburger">  Burger </NavigationItem>
                  
                  </div>
                  <div className={classes.column } >
                      <NavigationItem link="/pasta">   Pasta</NavigationItem>
                  </div>
                  <div className={classes.column } >
                     <NavigationItem link="/desserts">  Desserts</NavigationItem>
                  </div>
                  <div className={classes.column } >
                     <NavigationItem link="/kids"> Enfant</NavigationItem>
                  </div>
                  <div className={classes.column } >
                     <NavigationItem link="/Sandwich"> Sandwiches</NavigationItem>
                  </div>
            </div>
        </div>
        
   </div>
   
  </ul>
        <NavigationItem link="/" > burgerExtras </NavigationItem>
   <ul className={classes.NavigationItems3}>
        <NavigationItem link="/create-menus"> Creé un Menus</NavigationItem>
        <NavigationItem link="/orders">Commandes</NavigationItem>
        <NavigationItem link="/employes"> Personnel</NavigationItem>
   </ul>
        {/* <NavigationItem link="/employes"> S'identifier</NavigationItem>
        <NavigationItem link="/employes"> S'inscrire</NavigationItem> */}
   </ul>
);
 
export default navigationItems;


