import React, { Component } from 'react'
import classes from'./Employes.css'
import {NavLink} from 'react-router-dom'
import axios from 'axios';
 import Button from '../UI/Button/Button';
 //import DeleteEmployes from './DeleteEmployes/DeleteEmployes';
//import {Route} from 'react-router-dom'
//  import AddEmployes from './AddEmployes/AddEmployes';
//import { Button } from 'react-bootstrap';
// import Spinner from '../../components/UI/Spinner/Spinner';

// import Input from '../../components/UI/Input/Input';
export class Employes extends Component {
    constructor(){
        super()
        this.state={
            employes:[],
            isActive:false,
           
        }
    }
    componentDidMount()
  {
      
      axios.get('http://sandwitch.test/api/employes')
      .then(response=> {
          console.log(response.data)
          // console.log(this.props.match)
          this.setState({
              employes:response.data,
              
          });
      });
      
  }
    handleShow = ()=>{
       
        this.setState({
            isActive: true
        })
    //    this.props.history.replace('employes/add-employes')
       console.log(this.props.history)
       
    }
    handleHide = () =>{
        this.setState({
            isActive: false
        })
    }
   
    // deleteHandlerClick =()=>{
    //     axios.get('http://sandwitch.test/api/employes/')
    //     .then(response => {
    //        console.log(this.props);
    //        //this.setState({loading: true, order: null});
    //     });
    // }
    
   
    
    
   
   
    render() {
        const formElementsArray = [];
        for (let key in this.state.employesForm) {
            formElementsArray.push({
                id: key,
                config: this.state.employesForm[key]
            });
        }
       
        
        return (
            
<div className= {classes.tableWrapper}>

        <div style={{ 'textAlign':'left','fontSize':'10px','marginLeft':'5em','fontWeight':'bold' }} >
            <NavLink exact to={'/add-employes'} > <Button btnType="Success" > Ajouter un Employes</Button> </NavLink>
            {/* <Button btnType="Success" clicked={this.handleShow} > Ajouter un Employes</Button> */}
        </div>
            
        <div >
        <table className = {classes.flTable} >
            <thead>
                <tr>
                    <th></th>
                    <th style={{ 'fontSize':'10px' }}>Nom & Prénom </th>
                    <th style={{ 'fontSize':'10px' }}>Email</th>
                    <th style={{ 'fontSize':'10px' }}>Téléphone</th>
                    <th style={{ 'fontSize':'10px' }}>Salaire</th>
                    
                </tr>
            </thead>
                    <tbody>
                {
                    this.state.employes.map((employe,index) => {
                        return(
                    <tr key={index.id}>
                        <td> 
                            
                        </td>
                        
                            <td key={index.id}  style={{ 'fontSize':'10px' }}><NavLink style={{ 'textDecorationLine':'none','textDecorationStyle':'solid','color':'black','fontWeight':'bold' }} to = {"/employes/" + employe.id} >{employe.firstName} {employe.lastName}</NavLink> </td>
                            <td key={index.id}  style={{ 'fontSize':'10px','textDecorationLine':'none','textDecorationStyle':'solid','color':'black','fontWeight':'bold','fontFamily':'inherit' }}>{employe.email}</td>
                            <td key={index.id}style={{ 'fontSize':'10px' ,'textDecorationLine':'none','textDecorationStyle':'solid','color':'black','fontWeight':'bold','fontFamily':'inherit' }}>{employe.tel}</td>
                            <td key={index.id}  style={{ 'textDecorationLine':'none','fontSize':'10px' ,'textDecorationStyle':'solid','color':'black','fontWeight':'bold','fontFamily':'inherit' }}>{employe.salary} {employe.devise}</td>
                            
                    </tr>
                    )})

                }
                </tbody>
        </table>
        
        </div>
            {/* {this.state.isActive ? <AddEmployes 
            addEmployes = {this.employesDataHandler} />: null }  */}
</div>
            
        )}
}

export default Employes
