import React, { Component } from 'react'
//import Button from '../../UI/Button/Button';
import axios from 'axios'
import {connect} from 'react-redux'
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from '../EditEmployes/EditeEmployes.css';
import 'sweetalert2/src/sweetalert2.scss'
import Input from '../../../components/UI/Input/Input';
export class EditeEmployes extends Component {
    constructor(){
        super()
        this.state={
            employesForm: {
                firstName: {
                    
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Nom'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false
                },
                lastName: {
                    
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Prenom'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false
                },
                email: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'email',
                        placeholder: 'E-mail'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
                },
                tel: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'number',
                        placeholder: 'Numero de Téléphone'
                    },
                    value: '',
                    validation: {
                        required: true,
                        valid: false,
                        touched: false,
                        minLength: 8,
                        maxLength: 10
                      },
                      valid: false,
                      touched: false
                },
                salary: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'number',
                        placeholder: 'Salaire'
                    },
                    value: '',
                    validation: {
                        required: true,
                        valid: false,
                      touched: false,
                      minLength: 3,
                        maxLength: 3
                      },
                      valid: false,
                      touched: false
                },
                contrat: {
                    elementType: 'select',
                    elementConfig: {
                      options: [
                        {value: 'SIVP', displayValue: 'SIVP'},
                        {value: 'CDI', displayValue: 'CDI'},
                        {value: 'CDD', displayValue: 'CDD'},
                        
                      ]
                    },
                    value: 'EUR',
                    validation: {},
                    valid: true,
                    touched: false
                  } ,
                devise: {
                    elementType: 'select',
                    elementConfig: {
                      options: [
                        {value: 'EUR', displayValue: 'EUR'},
                        {value: 'USD', displayValue: 'USD'},
                        {value: 'TND', displayValue: 'TND'},
                        
                      ]
                    },
                    value: 'EUR',
                    validation: {},
                    valid: true,
                    touched: false
                  } ,
                address: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Adresse'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
    
                },
                pays: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Pays'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
    
                },
                
                birthday: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'date',
                        placeholder: 'Date de Naissance'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
                },
               
               
            },
            loading: false
        }
    }
    componentDidMount(){
        axios.get( `http://sandwitch.test/api/employes/`+ this.props.match.params.id)
        .then( res => {
            this.setState({  firstName:this.state.employesForm.firstName.value = res.data.firstName,
                lastName:this.state.employesForm.lastName.value=res.data.lastName,
                email:this.state.employesForm.email.value = res.data.email,
                tel:this.state.employesForm.tel.value = res.data.tel,
                salary:this.state.employesForm.salary.value=res.data.salary,
                devise:this.state.employesForm. devise.value=res.data.devise,
                contrat:this.state.employesForm. contrat.value=res.data.contrat,
                pays:this.state.employesForm.pays.value=res.data.pays,
                address:this.state.employesForm.address.value=res.data.address,
                birthday:this.state.employesForm.birthday.value=res.data.birthday, 
            });
            console.log(res.data)

            console.log(this.props)
         })
    }
    employesEditeHandler=()=>{
      
        const EmployesEdite = {
            
            firstName:this.state.employesForm.firstName.value,
            lastName:this.state.employesForm.lastName.value,
            email:this.state.employesForm.email.value,
            tel:this.state.employesForm.tel.value,
            salary:this.state.employesForm.salary.value,
            devise:this.state.employesForm.devise.value,
            pays:this.state.employesForm.pays.value,
            address:this.state.employesForm.address.value,
            birthday:this.state.employesForm.birthday.value,

          
           }
        axios.put(`http://sandwitch.test/api/employes/`+ this.props.match.params.id,EmployesEdite)
        .then(res=>
            
            console.log(res.data),
            this.props.history.replace('/employes/'+ this.props.match.params.id ))
    }
    checkValidity(value, rules) {
    
        let isValid =true;
        
        if (!rules) {
          return true;
        }
        
        if(rules.required){
          isValid = value.trim() !== '' && isValid;
        }
    
        if(rules.minLength) {
          isValid = value.length >= rules.minLength && isValid
        }
    
        if(rules.maxLength) {
          isValid = value.length <= rules.maxLength && isValid
        }
    
        return isValid;
      }
      inputChangedHandler = (event, inputIdentifier) => {
        const updatedEmployesForm = {
          ...this.state.employesForm
        };
        const updatedFormElement = { 
          ...updatedEmployesForm[inputIdentifier] 
        };
    
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedEmployesForm[inputIdentifier] = updatedFormElement;
        
        let formIsValid = true;
        for( let inputIdentifier in updatedEmployesForm) {
          formIsValid = updatedEmployesForm[inputIdentifier].valid && formIsValid;
    
        }
        this.setState({employesForm: updatedEmployesForm, formIsValid: formIsValid});
      };
  
  
      canceledHandler = ()=>{

        //console.log(this.props.history)
        this.props.history.push('/employes/'+this.props.match.params.id);

    }
    render () {
        const formElementsArray = [];
        for (let key in this.state.employesForm) {
            formElementsArray.push({
                id: key,
                config: this.state.employesForm[key]
            });
        }
        let form = (
        <form onSubmit={this.employesEditeHandler}>
            {formElementsArray.map(formElement => (
                <Input 
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                />
            ))}
            <Button btnType="Success" clicked={this.employesEditeHandler}>Enregistrer</Button>
            <Button btnType="Danger" clicked={this.canceledHandler}>Annuler</Button>
        </form>
        );
        if ( this.state.loading ) {
            form = <Spinner />;
        }
    return (

        <div className={classes.AddEmployes}>
            <h4>Modifier Les Données  de Votre Employé </h4>
            {form}
        </div>

        );
    }
}


    const mapStateToProps = state => {
        return {
        //   ings: state.ingredients,
        //   price: state.totalPrice
                // nom:state.firstName,
                // lastName:state.prenom,
                // email:state.email,
                // tel:state.telephone,
                // salary:state.salair,
                // birthday:state.date,
                // address:state.adresse,
                firstName:state.firstName,
                lastName:state.lastName,
                email:state.email,
                tel:state.tel,
                salary:state.salary,
                devise:state.devise,
                address:state.address,
                pays:state.pays,
                birthday:state.birthday,
               
               // employesData:formData.value
        }
    }
    
  
  export default connect(mapStateToProps)(EditeEmployes);