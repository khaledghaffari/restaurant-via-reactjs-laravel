// import React, { Component } from 'react'
// import axios from 'axios'
// export class AddEmployes extends Component {
    
//     constructor(){
//         super();
//         this.inputChangeHandler=this.inputChangeHandler.bind(this)
//         this.onSubmit = this.onSubmit.bind(this)
//         this.state={
//             name:''
//         }
//     }
//     inputChangeHandler = (event)=>{

//         this.setState({
//             name:event.target.value
//         });

//     }
//     onSubmit(event){

//         event.preventDefault();
//         const dataName = {
//             name : this.state.name
//         }
//         axios.post('http://127.0.0.1:8000/api/employes/'+dataName)
//         .then(res=>console.log(res.data))

//     }
//     render() {
//     return (
//         <div>
//                 <form onSubmit={this.onSubmit} >
//                     <div>
//                         <label for="name">Name :</label>
//                         <input type="text" 
//                         id="name"
//                         placeholder="name" 
//                         value={this.state.name}
//                         onChange={this.inputChangeHandler}
//                         />
//                     </div>
//                 <button type="submit">Submit</button>
//                 </form>
//         </div>
//         )
//     }
// }

// export default AddEmployes

import React, { Component } from 'react';
import {connect} from 'react-redux'
import Button from '../../../components/UI/Button/Button';
import Spinner from '../../../components/UI/Spinner/Spinner';
import classes from '../AddEmployes/AddEmployes.css';
import axios from 'axios';
import Input from '../../../components/UI/Input/Input';
//import Swal from 'sweetalert2/dist/sweetalert2.js'

import 'sweetalert2/src/sweetalert2.scss'
class AddEmployes extends Component {
    constructor(){
        super()
        this.state={
            employesForm: {
                firstName: {
                    
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Nom'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false
                },
                lastName: {
                    
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Prenom'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false
                },
                email: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'email',
                        placeholder: 'E-mail'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
                },
                tel: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'number',
                        placeholder: 'Numero de Téléphone'
                    },
                    value: '',
                    validation: {
                        required: true,
                        valid: false,
                        touched: false,
                        minLength: 8,
                        maxLength: 10
                      },
                      valid: false,
                      touched: false
                },
                salary: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'number',
                        placeholder: 'Salaire'
                    },
                    value: '',
                    validation: {
                        required: true,
                        valid: false,
                      touched: false,
                      minLength: 3,
                        maxLength: 3
                      },
                      valid: false,
                      touched: false
                },
                contrat: {
                  elementType: 'select',
                  elementConfig: {
                    options: [
                      {value: 'SIVP', displayValue: 'SIVP'},
                      {value: 'CDI', displayValue: 'CDI'},
                      {value: 'CDD', displayValue: 'CDD'},
                      
                    ]
                  },
                  value: 'EUR',
                  validation: {},
                  valid: true,
                  touched: false
                } ,
                devise: {
                    elementType: 'select',
                    elementConfig: {
                      options: [
                        {value: 'EUR', displayValue: 'EUR'},
                        {value: 'USD', displayValue: 'USD'},
                        {value: 'TND', displayValue: 'TND'},
                        
                      ]
                    },
                    value: 'EUR',
                    validation: {},
                    valid: true,
                    touched: false
                  } ,
                address: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Adresse'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
    
                },
                pays: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Pays'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
    
                },
                
                birthday: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'date',
                        placeholder: 'Date de Naissance'
                    },
                    value: '',
                    validation: {
                        required: true
                      },
                      valid: false,
                      touched: false
                },
               
               
            },
            loading: false
        }
    }
    
    employesDataHandler = (event) => {
      const Swal = require('sweetalert2');


      event.preventDefault();
      this.setState( { loading: true } );

      const formData = {};

      for (let formElementIdentifier in this.state.employesForm) {
          formData[formElementIdentifier] = this.state.employesForm[formElementIdentifier].value;
      }
      const newEmployes = {
        
          // ingredients: this.props.ingredients,
          // price: this.props.price,
          
          firstName:this.state.employesForm.firstName.value,
          lastName:this.state.employesForm.lastName.value,
          email:this.state.employesForm.email.value,
          tel:this.state.employesForm.tel.value,
          salary:this.state.employesForm.salary.value,
          contrat:this.state.employesForm.contrat.value,
          devise:this.state.employesForm.devise.value,
          pays:this.state.employesForm.pays.value,
          address:this.state.employesForm.address.value,
          birthday:this.state.employesForm.birthday.value,
          employesData:formData.value
         }
      Swal.fire({
        stitle: "Êtes-vous sure de ces Coordonnées ?",
        text: "Êtes-vous sure de ces Coordonnées  ?",
        icon: 'warning',
        width: 500,
        fontSize: 50,
        showCancelButton: true,
        confirmButtonColor: '#5C9210',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ajouter'
      }).then((result) => {
        if (result.isConfirmed) {

          Swal.fire(
            'Ajouter!',
            'Votre employée a été Ajouter avec succès. ',
            'success',
      axios.post('http://sandwitch.test/api/employes' , newEmployes )
          .then( response => {
                console.log(response)
                this.setState( { loading: false } );
                this.props.history.push('/employes/');
          })
          )
                }
            })
          
          .catch( error => {
              this.setState( { loading: false } );
              console.log(error)
          });
          
  }
  
   
    checkValidity(value, rules) {
    
        let isValid =true;
        
        if (!rules) {
          return true;
        }
        
        if(rules.required){
          isValid = value.trim() !== '' && isValid;
        }
    
        if(rules.minLength) {
          isValid = value.length >= rules.minLength && isValid
        }
    
        if(rules.maxLength) {
          isValid = value.length <= rules.maxLength && isValid
        }
    
        return isValid;
      }
      inputChangedHandler = (event, inputIdentifier) => {
        const updatedOrderForm = {
          ...this.state.employesForm
        };
        const updatedFormElement = { 
          ...updatedOrderForm[inputIdentifier] 
        };
    
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedOrderForm[inputIdentifier] = updatedFormElement;
        
        let formIsValid = true;
        for( let inputIdentifier in updatedOrderForm) {
          formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    
        }
        this.setState({employesForm: updatedOrderForm, formIsValid: formIsValid});
      };
    // inputChangedHandler = (event, inputIdentifier) => {
    //     const updatedOrderForm = {
    //         ...this.state.employesForm
    //     };
    //     const updatedFormElement = { 
    //         ...updatedOrderForm[inputIdentifier]
    //     };
    //     updatedFormElement.value = event.target.value;
    //     updatedOrderForm[inputIdentifier] = updatedFormElement;
    //     this.setState({employesForm: updatedOrderForm});
    // }
    canceledHandler = ()=>{

        //console.log(this.props.history)
        this.props.history.push('/employes');

    }

    render () {
        const formElementsArray = [];
        for (let key in this.state.employesForm) {
            formElementsArray.push({
                id: key,
                config: this.state.employesForm[key]
            });
        }
        let form = (
        <form onSubmit={this.employesDataHandler}>
            {formElementsArray.map(formElement => (
                <Input 
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                changed={(event) => this.inputChangedHandler(event, formElement.id)}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                />
            ))}
            <Button btnType="Success" disabled={!this.state.formIsValid} clicked={this.employesDataHandler}>Ajouter</Button>
            <Button btnType="Danger" clicked={this.canceledHandler}>Annuler</Button>
        </form>
        );
        if ( this.state.loading ) {
            form = <Spinner />;
        }
    return (

        <div className={classes.AddEmployes}>
            <h4>Entrer Les Données  de Votre Employé </h4>
            {form}
        </div>

        );
    }
}


    const mapStateToProps = state => {
        return {
        //   ings: state.ingredients,
        //   price: state.totalPrice
                // nom:state.firstName,
                // lastName:state.prenom,
                // email:state.email,
                // tel:state.telephone,
                // salary:state.salair,
                // birthday:state.date,
                // address:state.adresse,
                firstName:state.firstName,
                lastName:state.lastName,
                email:state.email,
                tel:state.tel,
                salary:state.salary,
                devise:state.devise,
                address:state.address,
                pays:state.pays,
                birthday:state.birthday,
               
               // employesData:formData.value
        }
    }
    
  
  export default connect(mapStateToProps)(AddEmployes);