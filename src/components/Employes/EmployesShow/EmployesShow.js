import React, { Component } from 'react'
import axios from 'axios';
import classess from '../EmployesShow/EmployesShow.css';
import Button from '../../UI/Button/Button'
//import Button from 'react-bootstrap/Button';
//import { Modal } from 'react-bootstrap';
//import Swal from 'sweetalert2/dist/sweetalert2.js'
import {NavLink} from 'react-router-dom'
import 'sweetalert2/src/sweetalert2.scss'
//import EditeEmployes from '../EditEmployes/EditeEmployes'
import { Icon, InlineIcon } from '@iconify/react';
import arrowBackSharp from '@iconify/icons-ion/arrow-back-sharp';
export class EmployesShow extends Component {

    constructor(){
        super()
        this.state={
            employeshow:[],
            isActive: false,
            show:false,
            loading: false
        }
       
    }
     
    handleShow =()=>{
       
       // this.setState({ isActive: true ,show:true})
     
    }
 
    handleHide = () => {
        this.setState({
            isActive: false,
            show:false
            
        })
    }
    componentDidMount()
    {
        axios.get(`http://sandwitch.test/api/employes/`+ this.props.match.params.id)
        .then( res => 
            {
             this.setState({ employeshow:res.data });
             console.log(res.data)
             console.log(this.props)
            })
         
    }
        editeHanlderClick(){
            this.props.history.push('/edit/'+this.props.match.params.id);
        }
        clickBackHandler(){
           // console.log(this.props)
            this.props.history.push('/employes');
        }    
        deleteHandlerClick(){
            
            const Swal = require('sweetalert2');
            Swal.fire({
                stitle: 'Êtes-vous sure de supprimer cette employée ?',
                text: "Êtes-vous sure de supprimer cette employée ?",
                icon: 'warning',
                width: 900,
                fontSize: 50,
                showCancelButton: true,
                confirmButtonColor: '#5C9210',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Supprimer'
              }).then((result) => {
                if (result.isConfirmed) {
                    
                  Swal.fire(
                    'Supprimé!',
                    'Votre employée a été supprimée avec succès. ',
                    'success',
                    axios.delete( `http://sandwitch.test/api/employes/`+this.props.match.params.id)
                    .then( res => {
                        this.props.history.push('/employes');
                        this.setState( { loading: true } );
                    })
                  )
                }
            })
          
             
        }
         render() {

        return (
            <div className={classess.employes}>
                 <div className={classess.container}><button className={classess.Button} style= {{ }} onClick={()=> this.clickBackHandler()} > <Icon icon={arrowBackSharp} /> Retour</button>  
               </div>
                 
             <div style= {{ 'textAlign':'left','fontSize':'20px','marginTop':'3em','marginLeft':'12em' }} >
                
             <h1 style= {{ 'fontFamily':'fantasy','fontSize':'45px'}}> {this.state.employeshow.firstName} {this.state.employeshow.lastName} 
                 </h1>
                 <hr/>
                 <p> <span style={{ 'fontWeight':'bold' }} >Nom : </span> {this.state.employeshow.firstName} </p>
                <p> <span style={{ 'fontWeight':'bold' }} >Prenom : </span> {this.state.employeshow.lastName} </p>
                <p> <span style={{ 'fontWeight':'bold' }} >Email : </span> {this.state.employeshow.email} </p>
                <p> <span style={{ 'fontWeight':'bold' }} >Telephone : </span> {this.state.employeshow.tel} </p>
                <p> <span style={{ 'fontWeight':'bold' }} >Salaire : </span> {this.state.employeshow.salary} {this.state.employeshow.devise} </p>
                <p> <span style={{ 'fontWeight':'bold' }} >Type de Contart : </span> {this.state.employeshow.contrat}  </p>
                <hr/>
                
                <p> <span style={{ 'fontWeight':'bold' }} >Date de Naissance : </span> {this.state.employeshow.birthday} -{this.state.employeshow.pays}  </p>
                <p> <span style={{ 'fontWeight':'bold' }} >Adresse : </span>{this.state.employeshow.address} </p>
                
               
             </div>

        <div style= {{'fontSize':'25px','paddingTop':'3em'}}>
               
            <Button btnType="Danger"  clicked={ ()=> this.deleteHandlerClick() }> Supprimer</Button>
            <Button btnType="Success" > <NavLink to= {'/edit/'+this.props.match.params.id} > Modifier</NavLink> </Button>      
                
           
                
        </div>
           </div>
        
        )

    }
} 

export default EmployesShow

