import React, { Component } from 'react'
import classes from '../CarteMenu/CarteMenu.css'
import {NavLink} from 'react-router-dom'
export class CarteMenu extends Component {
    render() {
        return (
            <div>
                <div className={classes.grid}>
                    <NavLink to='/hamburger'>
                    <div className={classes.hamburger} ><h2 className={classes.hamburgerTitles}>Hamburger</h2> </div>
                    </NavLink>
                    <NavLink to='/pasta'>
                    <div className={classes.pasta} ><h2 className={classes.pastaTitles}>Pasta</h2></div>
                    </NavLink>
                    <NavLink to='Sandwich'>
                    <div className={classes.sandwich} ><h2 className={classes.sandwichTitles}>Sandwiches</h2></div>
                    </NavLink>
                    <NavLink to='desserts'>
                    <div className={classes.dessert} ><h2 className={classes.dessertTitles}>Dessert</h2></div>
                    </NavLink>
                    <NavLink to='kids'>
                    <div className={classes.kids}  ><h2 className={classes.kidsTitles}>Menu Enfant</h2></div>
                    </NavLink>
                       
                       
                       
                       
                      

                </div>
            </div>
        )
    }
}

export default CarteMenu
