import React, { Component } from 'react'
import classes from '../Dessert/Dessert.css'

export class Dessert extends Component {
    render() {
        return (
            <div className = {classes.image}>
                <img src={this.props.imageUrl}/>
                <h2 className={classes.titles}> {this.props.titles} </h2>
                <p className={classes.text}> {this.props.texte} </p>
                
                <span className={classes.price}> $ {this.props.prices} </span>
                <button className={classes.btn}> Commander</button>
            </div>
        )
    }
}

export default Dessert
