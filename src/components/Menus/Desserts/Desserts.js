import React, { Component } from 'react'
import  classes from'../Desserts/Desserts.css'
import Desert from './Dessert/Desert'
import {NavLink} from 'react-router-dom'
import {db} from '../../../firebase/fireBase';
import { Icon, InlineIcon } from '@iconify/react';
import arrowBackSharp from '@iconify/icons-ion/arrow-back-sharp';

export class Desserts extends Component 
{
    
 constructor()
 {
    super()
    this.state = {
      Desserts:[]
      }

 }
 componentDidMount = () =>
  {
      db.collection('Desserts').onSnapshot(snapshot=>
      {
          this.setState({
              Desserts:snapshot.docs.map(doc => doc.data())
          })
      })
  }

    render() 
    {
        return (
           
        <div >
             <h1 className={classes.titre}> Dessert </h1>
             <NavLink to='/carte'><button className={classes.Button} > <Icon icon= {arrowBackSharp} /> Retour a La Carte</button>  
                     </NavLink>
             <div  className={classes.grid}>
             { 
            this.state.Desserts.map((Dessert,id)=>{
                return(

                 <NavLink key={id} to= "#">
                 <Desert
                          index = {Dessert.id}
                          imageUrl = {Dessert.imageUrl}
                          titles = {Dessert.titles}
                          texte =  {Dessert.texte}
                          prices = {Dessert.prices}
                        />
                 </NavLink>
                
                )})  
        } 
             </div>
        
        </div>
        )

    }

}

export default Desserts

