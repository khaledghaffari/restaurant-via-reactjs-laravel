import React, { Component } from 'react'
import classes from '../Pasta/Pasta.css'

export class Pasta extends Component {
    render() {
        return (
                <div className = {classes.image}>
                    
                    <img src={this.props.imageUrl}/>
                    <h2 className={classes.titles}> {this.props.titles} </h2>
                    <p className={classes.text}> {this.props.texte} </p>

                    <span className={classes.price}>$ {this.props.prices} </span>
                    <button className={classes.btn}> Commander</button>
                
                </div>
                 )
                 
    }
}
export default Pasta