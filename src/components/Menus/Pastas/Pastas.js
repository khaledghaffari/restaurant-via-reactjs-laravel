import React, { Component } from 'react'
import  classes from'../Pastas/Pastas.css'
import Pasta from './Pasta/Pasta'
import {NavLink} from 'react-router-dom'

import {db} from '../../../firebase/fireBase';
import { Icon, InlineIcon } from '@iconify/react';
import arrowBackSharp from '@iconify/icons-ion/arrow-back-sharp';


export class Pastas extends Component 
{
    
 constructor()
 {
    super()
    this.state = 
    {
    pastas:[]
    }

 }
 componentDidMount = () =>
{
    db.collection('Pastas').onSnapshot(snapshot=>
     {
        this.setState({
            pastas:snapshot.docs.map(doc => doc.data())
        })
    })
}

    render() 
    {
        return (
        <div >
             <h1 className={classes.titre}> Pasta </h1>
             <NavLink to='/carte'><button className={classes.Button} > <Icon icon= {arrowBackSharp} /> Retour a La Carte</button>  
                     </NavLink>
            <div className = {classes.grid} >
           
            { 
            
            this.state.pastas.map((pasta,id)=>{
                return(

                 <NavLink key={id} to= "#">
                 <Pasta
                          index = {pasta.id}
                          imageUrl = {pasta.imageUrl}
                          titles = {pasta.titles}
                          texte =  {pasta.texte}
                          prices = {pasta.prices}
                        />
                 </NavLink>
                
                )})  
        } 
            </div>
        
        </div>
        )

    }

}

export default Pastas

