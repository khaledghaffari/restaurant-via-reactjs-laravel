import React, { Component } from 'react'
import  classes from'./Hamburgers.css'
import Hamburger from './Hamburger/Hamburger'
import {NavLink} from 'react-router-dom'
import {db} from '../../../firebase/fireBase';
import { Icon, InlineIcon } from '@iconify/react';
import arrowBackSharp from '@iconify/icons-ion/arrow-back-sharp';

export class Hambuerger extends Component 
{
    
 constructor(props)
 {
    super()
    this.state = 
    {
        test:'',
    Hambuergers:[]
    }

 }
 componentDidMount = (props) =>
{

    db.collection('Hamburgers').onSnapshot(snapshot=>
    {
        this.setState({
            Hambuergers:snapshot.docs.map(doc => ({
                id:doc.id,
                Hambuerger:doc.data()
            }))
        })
       // console.log( db.collection('Hamburgers').doc('SRJWZYVMDmsSSyGhuCf2'))
        console.log(this.props.match)
     })
}

    render() 
    {
        return (
        <div>
             <h1 className={classes.titre}> Hamburger</h1>
             <NavLink to='/carte'><button className={classes.Button} > <Icon icon= {arrowBackSharp} /> Retour a La Carte</button>  
                     </NavLink>
          <div className = {classes.grid}>
            { 
            this.state.Hambuergers.map(({Hambuerger,id})=>{
                return(
                      

                 <NavLink key={id} to= {'/hamburger/'+ Hambuerger.id } >

                 <Hamburger
                    index = {Hambuerger.id}
                    imageUrl = {Hambuerger.imageUrl}
                    titles = {Hambuerger.titles}
                    texte =  {Hambuerger.texte}
                    prices = {Hambuerger.prices}
                 />

                </NavLink>
                
                )
            })  
                
            } 
          </div>
        
        </div>
        )

    }

}

export default Hambuerger

