import React, { Component } from 'react'
import classes from'./Sandwichs.css';
import Sandwich from './Sandwich/Sandwich'
import {NavLink} from 'react-router-dom'
import {db} from '../../../firebase/fireBase';
import { Icon, InlineIcon } from '@iconify/react';
import arrowBackSharp from '@iconify/icons-ion/arrow-back-sharp';

export class Sandwichs extends Component 
{
    
 constructor()
 {
    super()
    this.state = 
    {
     Sandwichs:[]
    }

 }
 componentDidMount = () =>
    {
        db.collection('Sandwich').onSnapshot(snapshot=>
        {
            this.setState({
                Sandwichs:snapshot.docs.map(doc => doc.data())
            })
        })
    }

    render() 
    {
        return (
        <div >
           
                    
                    <h1 className={classes.titre}> Sandwichs </h1>
                   
            <div className = {classes.grid} >
                    <NavLink to='/carte'><button className={classes.Button} > <Icon icon= {arrowBackSharp} /> Retour a La Carte</button>  
                     </NavLink>
            
                    { 
                    this.state.Sandwichs.map((sandwich,id)=>{
                        return(

                        <NavLink key={id} to= "#">
                        <Sandwich
                                index = {sandwich.id}
                                imageUrl = {sandwich.imageUrl}
                                titles = {sandwich.titles}
                                texte =  {sandwich.texte}
                                prices = {sandwich.prices}
                                />
                        </NavLink>
                        
                        )})  
                } 
            </div>
        
        </div>
        )

    }

}

export default Sandwichs

