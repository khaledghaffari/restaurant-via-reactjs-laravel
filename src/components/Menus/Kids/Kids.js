import React, { Component } from 'react'
import  classes from'../Kids/Kids.css'
import Kid from './Kid/Kid'
import {NavLink} from 'react-router-dom'
import {db} from '../../../firebase/fireBase';

import { Icon, InlineIcon } from '@iconify/react';
import arrowBackSharp from '@iconify/icons-ion/arrow-back-sharp';
export class Kids extends Component 
{
    
 constructor()
 {
    super()
    this.state = 
    {
    kids:[]
    }

 }
 componentDidMount = () =>
{
    db.collection('Kids').onSnapshot(snapshot=>
     {
        this.setState({
            kids:snapshot.docs.map(doc => doc.data())
        })
    })
}

    render() 
    {
        return (
        <div >
             <h1 className={classes.titre}> Menus Enfant </h1>
             
            <div className = {classes.grid} >
            <NavLink to='/carte'><button className={classes.Button} > <Icon icon= {arrowBackSharp} /> Retour a La Carte</button>  
                     </NavLink>
            { 
            this.state.kids.map((kid,id)=>{
                return(

                 <NavLink key={id} to= "#">
                 <Kid
                          index = {kid.id}
                          imageUrl = {kid.imageUrl}
                          titles = {kid.titles}
                          texte =  {kid.texte}
                          prices = {kid.prices}
                        />
                 </NavLink>
                
                )})  
        } 
            </div>
       
        </div>
        )

    }

}

export default Kids

