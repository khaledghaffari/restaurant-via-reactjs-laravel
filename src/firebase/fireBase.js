import firebase from 'firebase';

import 'firebase/storage';
import 'firebase/firebase-database';

  
  // Initialize Firebase
  const firebaseApp = firebase.initializeApp({ 

      apiKey: "AIzaSyCVx6KBvJ7WWQg9IVbQuBhUlDPV4BehGCI",
      authDomain: "burger-f94d7.firebaseapp.com",
      databaseURL: "https://burger-f94d7.firebaseio.com",
      projectId: "burger-f94d7",
      storageBucket: "burger-f94d7.appspot.com",
      messagingSenderId: "1030283993245",
      appId: "1:1030283993245:web:30778baa0040e2e80b3474",
      measurementId: "G-G0VG7NBXNL",

   });

   const db = firebaseApp.firestore();
   const storage = firebase.storage();
 
   export { db,storage }