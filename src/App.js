import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom'
import Layout from './components/Layout/Layout'
import SandwichMaker from './containers/SandwichMaker/SandwichMaker'
import Checkout from './containers/Checkout/Checkout'
import Orders from './containers/Orders/Orders'
import Employes from './components/Employes/Employes';
import EmployesShow from './components/Employes/EmployesShow/EmployesShow'
import Desserts from './components/Menus/Desserts/Desserts';
import Pasta from './components/Menus/Pastas/Pastas';
import Hamburger from './components/Menus/Hambuergers/Hambuergers';
import AddEmployes from './components/Employes/AddEmployes/AddEmployes';
import EditeEmployes from './components/Employes/EditEmployes/EditeEmployes'
import CreateMenus from './components/Menus/CreateMenus/CreateMenus';
import HamburgerShow from './components/Menus/Hambuergers/HamburgerShow/HamburgerShow'
import Kids from './components/Menus/Kids/Kids'
import Sandwichs from './components/Menus/Sandiwichs/Sandwichs'
import CarteMenus from './components/Menus/CarteMenus/CarteMenus'
class App extends Component {


  render() {
    
    return (
      <div>
        <Layout>
            <Switch>
                <Route path="/checkout" component={Checkout} />
                <Route path="/orders/" exact component={Orders} />
                <Route path="/" exact component={SandwichMaker} />
                <Route path="/employes" exact component={Employes} />
                <Route path="/employes/:id" exact component={EmployesShow} />
                <Route path="/desserts" exact component={Desserts} />
                <Route path="/pasta" exact component={Pasta} />
                <Route path="/kids" exact component={Kids} />
                <Route path="/Sandwich" exact component={Sandwichs} />
                <Route path="/edit/:id" exact component={EditeEmployes} />
                <Route path="/hamburger" exact component={Hamburger} />
                <Route path="/carte" exact component={CarteMenus} />
                <Route path="/hamburger/:id" exact component={HamburgerShow} />
                <Route path="/add-employes/"  exact component={AddEmployes}/>
                <Route path="/create-menus" exact component={CreateMenus} />
            </Switch>    
        </Layout>
      </div>
    );
  }
}

export default App;
